FROM test-api:latest

ADD index.html /api/static/index.html

EXPOSE 8081

WORKDIR /api

ENTRYPOINT ["./server"]